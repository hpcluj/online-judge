'use strict';

angular.module('myApp.view2', ['ngRoute', 'ngCookies'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'problemsListController'
  });
}])

.controller('problemsListController', ['$scope', '$location', '$http', '$cookieStore','problemsService', 'backendUrlService', function($scope, $location, $http, $cookieStore, problemsService, backendUrlService) {
    
    problemsService.setProblems("");
    $scope.user = $cookieStore.get('name');
    
    $scope.changeView = function(view, index) {
        problemsService.setProblemIndex(index);
        $location.path(view);
    }
    
    $scope.problems = [];
    
    $http.get(backendUrlService.getProblems() + "?userId=" + $cookieStore.get('id')).success(function(data) {
        $scope.problems = data;
        problemsService.setProblems(data);
    });                            
    
}]);