package com.hp.tools.judge.services;

import com.hp.tools.judge.entities.AgentInput;
import com.hp.tools.judge.entities.AgentResult;
import com.hp.tools.judge.entities.Score;
import com.hp.tools.judge.evaluators.OutputEvaluator;
import com.hp.tools.judge.evaluators.OutputEvaluatorFactory;
import com.hp.tools.judge.utils.ConnectionDetails;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Octavian
 * @since 21.03.2015
 */
public class AgentService {
    private final static Logger LOGGER = Logger.getLogger(AgentService.class.getName());

    private ExecutorService executor;
    private ConnectionDetails connectionDetails;
    private Client httpClient;

    public AgentService(ConnectionDetails connectionDetails, ExecutorService executor) {
        this.connectionDetails = connectionDetails;
        this.executor = executor;
        this.httpClient = ClientBuilder.newClient();
        httpClient.property(ClientProperties.CONNECT_TIMEOUT, Long.toString(this.connectionDetails.getConnectTimeout()));
        httpClient.property(ClientProperties.READ_TIMEOUT, Long.toString(this.connectionDetails.getReadTimeout()));
    }

    public void compile(final AgentInput input, final Callback callback) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                LOGGER.info("Connecting to agent at " + connectionDetails.getUrl());

                Score score = new Score(0f, "");

                try {
                    WebTarget target = httpClient.target(connectionDetails.getUrl());
                    Response response = target.request(MediaType.APPLICATION_JSON)
                            .post(Entity.entity(input, MediaType.APPLICATION_JSON_TYPE));

                    AgentResult result = response.readEntity(AgentResult.class);
                    int returnCode = result.getReturnCode();
                    switch (returnCode) {
                        case 0:
                            score.setValue(100);
                            score.setDescription("Success");
                            break;
                        case 3:
                            score.setDescription("Compiler Error");
                            break;
                        case 4:
                            OutputEvaluator evaluator = OutputEvaluatorFactory.buildEvaluator(input.getLanguage());
                            score.setValue(evaluator.evaluate(result.getStderr()));
                            score.setDescription("Success");
                            break;
                        case 5:
                            score.setDescription("Time limit exceeded");
                            break;
                        case 6:
                            score.setDescription("Time limit exceeded");
                            break;
                        default:
                            score.setDescription("Other Error: " + returnCode);
                            LOGGER.severe(result.getStderr());
                    }
                } catch (Exception e) {
                    score.setDescription("Agent Generic Error");
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                }

                LOGGER.info("Send score back to the caller");
                callback.call(score);
            }
        });
    }

    public interface Callback {
        void call(Score score);
    }
}
