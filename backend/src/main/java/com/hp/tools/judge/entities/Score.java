package com.hp.tools.judge.entities;

/**
 * @author Octavian
 * @since 21.03.2015
 */
public class Score {
    private float value;
    private String description;

    public Score() {
    }

    public Score(float value, String description) {
        this.value = value;
        this.description = description;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return value + " - " + description;
    }
}
