package com.hp.tools.judge.evaluators;

/**
 * @author Octavian
 * @since 23.03.2015
 */
public class JavaOutputEvaluator extends RegexOutputEvaluator {
    public JavaOutputEvaluator() {
        super("Tests run: (\\d),  Failures: (\\d)");
    }
}
