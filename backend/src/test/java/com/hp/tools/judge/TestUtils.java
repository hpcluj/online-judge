package com.hp.tools.judge;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Octavian
 * @since 23.03.2015
 */
public class TestUtils {
    public static String getFileContents(String uri) throws URISyntaxException, IOException {
        return new String(Files.readAllBytes(getPath(uri)), "utf-8");
    }

    private static Path getPath(String uri) throws URISyntaxException {
        Path path = Paths.get(uri);
        if (!path.isAbsolute()) {
            path = Paths.get(TestUtils.class.getProtectionDomain().getCodeSource().getLocation().toURI()).resolve(path);
        }
        return path;
    }
}
