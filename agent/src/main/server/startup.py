#!/usr/bin/env python


import web

from main.api.greeting_impl import Greeting
from main.api.test_code_impl import TestCode


def notfound():
    return web.notfound("Method not found.")


def internalerror():
    return web.internalerror("Internal Agent Error: ")


urls = (
    "/agent", Greeting,
    "/agent/test", TestCode
)

app = web.application(urls, globals())
app.notfound = notfound
# app.internalerror = internalerror


if __name__ == "__main__":
    app.run()